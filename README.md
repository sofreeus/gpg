# GPG for signing 
*requirements:  Linix or Mac with `git` installed (Homebrew installed on Mac)*  
*NOTE: GPG can be used for encrypting/decrypting data, but that is a seperate use-case for GPG than what we will be doing today.*



## 1) The old way. Hashes
A hash has a relationship to a file. It's the "digital fingerprint" or "signature" of a file. You may be familiar with Linux hash algoriths, like SHA and MD5, where you can have a fingerprint file, along side it's data file, to ensure the integrity of the data file. Similarly, I'm sure you've downloaded software with a separate checksum. If the checksums don't match, you may have an incomplete or corrupted download. These concept are similar to what we will be doing today with Gnu Privacy Guard  
&nbsp;  
&nbsp;  


## 2) Downdoad GPG (if not already installed)
It should already exist on Linux, but if it doesn't, `dnf install gnupg` on Fedora, and `apt install gnupg` on Debian/Ubuntu.

For Mac, `brew install gnupg`  
&nbsp;  
&nbsp;  
  

## 3) Generate GPG key pair
### Create
  `gpg --full-generate-key`

- Chose type RSA

- Override default key size, and make 4096. 
- I gave mine a 2y expiration

- Enter your real name or what ever name you want to appear in GH

- Enter your commit email address (find out with git config --global user.email).  
*Later we will be adding the same email to Git (git config --global --edit)*

- Enter something like "for signing Git commits to SFS" or whatever, as comment. 

### See the public key you just created       
(it should have 3 sections: pub, uid & sub, and should reflect that you set it to expire in 2 years.)

     gpg --list-keys --keyid-format LONG

mine has this...


```
pub   rsa4096/69............4B 2021-10-13 [SC] [expires: 2023-10-13]
    7899C9DEB4C20EB7DC.....................
uid                 [ultimate] E. Richard Glazier (for signing commits to GL) erglazier@hotmail.com
sub   rsa4096/37.............F5 2021-10-13 [E] [expires: 2023-10-13
```
&nbsp;  
&nbsp;  


## 4) Configure Git
### Add this "pub" GPG Key ID to your global git config, so you can sign Git commits
`git config --global --edit`

Under the [user] stanza, you should have a user name and email. Add a line for `signingkey`
Mine looks like...

```
[user]
    name = E. Richard Glazier
    email = erglazier@hotmail.com
    signingkey = 69............4B


```
&nbsp;  
&nbsp;  

## 5) Sign local commits
You are now able to sign local git commits using the **-S** flag. 
(ex. `git commit -S -m"this is a test git commit"`)  
(install a 'pinenry' program if necessary. See 'extra curricular' section)  
(You can have git automatically sign git commits, but this is not my preference. See "Extra Curricular" section for information on this)    
&nbsp;  
&nbsp;  

## 6) Verify local commits
`git verify-commit` 

Example of how to see that a local commit is signed by GPG (git verify-commit will have output if it is not signed).  
Note that after `git commit -S -m"sign"`, I was prompted with a pop-up to enter my GPG passphrase. Since this example is from a Mac, the `pinentry` program is what took the passphrase (installed from homebrew). 
```
pwd
/Users/rich.glazier/gittest
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest
╰─➤  git init .
...
Initialized empty Git repository in /Users/rich.glazier/gittest/.git/
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  touch nosign
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master*›
╰─➤  git add .
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master*›
╰─➤  git commit -m"no sign"
[master (root-commit) abd1de0] no sign
  1 file changed, 0 insertions(+), 0 deletions(-)
  create mode 100644 nosign
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  git log
commit abd1de0e9b68a2272672b29f840bfe982c5b3592 (HEAD -> master)
Author: E. Richard Glazier richard.glazier@inteliquent.com
Date:   Thu Oct 14 09:47:30 2021 -0600

  no sign
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  git verify-commit abd1de0e9b68a2272672b29f840bfe982c5b3592
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  touch sign                                                                                                   1 ↵
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master*›
╰─➤  git add .
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master*›
╰─➤  git commit -S -m"sign"
[master d3daefa] sign
  1 file changed, 0 insertions(+), 0 deletions(-)
  create mode 100644 sign
╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  git log
commit d3daefa0942b8f1a29f6a1eb603ee9f86726156c (HEAD -> master)
Author: E. Richard Glazier richard.glazier@inteliquent.com
Date:   Thu Oct 14 09:48:53 2021 -0600

    sign

commit abd1de0e9b68a2272672b29f840bfe982c5b3592
Author: E. Richard Glazier richard.glazier@inteliquent.com
Date:   Thu Oct 14 09:47:30 2021 -0600

    no sign

╭─rich.glazier@Richs-MacBook-Pro.local ~/gittest  ‹master›
╰─➤  git verify-commit d3daefa0942b8f1a29f6a1eb603ee9f86726156c
gpg: Signature made Thu Oct 14 09:48:53 2021 MDT
gpg:                using RSA key 7899C9DEB4C20EB7DC08EEC46951EB4A5028804B
gpg: Good signature from "E. Richard Glazier (Non Yubikey key for signing commits) 
<richard.glazier@inteliquent.com>" [ultimate]
```
&nbsp;  
&nbsp;  

## 7) Push commits to GitLab

### Use the GPG key ID to output the public key.
The key ID is the "pub" line key, after rsa4096/        
In my case, from above, it's the one that starts 69 and ends 4B (69............4B)

This will output the public key:        `gpg --armor --export 69............4B`        
Copy the entire thing, including the -----BEGIN and -----END lines.  
(--armor converts from binary to 'ASCII-armored'. It's basically what gives you the real public key, from the KeyID)

### Share public key with GL profile
click profile drop-down in upper right, and then select "Edit profile".  
Select "GPG Keys" from the left pane.  
Paste the key and save.   

### Now go through entre git flow, to get a commit up to GL that is "verified"


&nbsp;  
&nbsp;  
## Extra Curricular

### -Sign a local (non git) file
`echo SFS > test.txt`  
`gpg --detach-sign test.txt`  
(install a 'pinenry' program if necessary. See 'extra curricular' section)  
`ls -l` (see the signatue file)  
`gpg --verify test.txt.sig`  
&nbsp;  
&nbsp;  

### -What happens when you remove your "email" line from your global git config, and try your git flow to GL again (add/commit_sign/push)?
&nbsp;  
### -Pinentry
On Mac, you could get a TUI pop-up to enter your GPG passphrase, or depending on the program using GPG, you may want/need to install a GUI program for entering a pin.

`brew install pinentry`
`which pinentry`
`echo "pinentry-program <path to pinentry>" > ~/.gnupg/gpg-agent.conf` 
&nbsp;  
&nbsp;  

### -Have git automatically sign commits (so you don't have to specify -S)
I prefer to be purposeful about what I do, each time I do it. But you may vi your global git config, and add a line, or inject it with `git config --global commit.gpgsign true`, if you wish to save the "-S" 2 keystrokes. You can google more about why you might or might not do that, and how it's actually git tags that get signed.... rabbit hole. 
&nbsp;  
&nbsp;  


### -git config
I like using `git config --global` with a specific parameter, the way I like using `visudo` or `vipw`. It does some syntax checking, and will insert lines with corect spacing (vs. manually editing the global git config file with nano / vi, or `git config --global --edit`)
&nbsp;  
&nbsp;  


### -What happens when you remove your email from your git config file, and try to sign/push?
&nbsp;  

### -Other ways to sign with GPG
What are 3 'sign' flags to gpg and how are they different?
&nbsp;  
&nbsp;  


### -Other ways to view GPG info
Does `gpg --list-keys` list the public or secret(private) keys?  
How would you list the secret keys (oops, I gave away the answer to the first one!)?  
What is the difference between `gpg --list-keys` and `gpg --list-keys --keyid-format LONG`?
&nbsp;  
&nbsp;  


### -export GPG keys to save a backup
~~google~~ DDG that sh*t



